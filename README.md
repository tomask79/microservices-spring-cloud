# Writing MicroServices [part 1] #

## MicroServices with Spring Cloud based on Netflix solution ##

One week ago at [Geecon 2016](http://2016.geecon.cz/schedule/) there were a lot of presentations about doing MicroServices and how great it is. Fact is that there are a lot of frameworks designated for it. At Geecon, Burr Sutter mentioned really interesting [Vert.X](http://vertx.io/) with support of shared data grid via [HazelCast](https://hazelcast.com/) between the microservices nodes. Or [Payra Microservices](http://blog.payara.fish/introducing-payara-micro) introduced by Ondrej Mihályi. Anyway very popular in these days is a solution provided by [Spring Cloud based on NetFlix solution](https://spring.io/blog/2015/07/14/microservices-with-spring). So let's make a demo of howto use it.

### Discovery Server (Eureka Server) ###

Hearth of MicroServices based on Spring Cloud is an **Eureka Server**. Also called **Discovery Server**. Because this server holds information about all microservices your system can use in terms of where they're running, about they health and other things. It's clear that in the production this server needs to be accessible with high availability. With Spring Cloud you create this server by adding **EnableEurekaServer** annotation to your start class of Spring Boot application.


```
@SpringBootApplication
@EnableEurekaServer
public class SpringMicroserviceRegistryApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMicroserviceRegistryApplication.class, args);
	}
}
```
This server is being **looked up by your microservices at http://localhost:8761 by default**. So just for fun, let's change it a little bit and start it at different port:


```
server.port=9761

eureka.client.register-with-eureka=false
eureka.client.fetch-registry=false

logging.level.com.netflix.eureka=OFF
logging.level.com.netflix.discovery=OFF

```

* **setting of eureka.client.register-with-eureka and eureka.client.fetch-registr** parameters to false says to registry server not to register himself a service.
* Our setting of **logging.level.com.netflix.eureka and logging.level.com.netflix.discovery** switches off complains about not having replica node for registry server.

### MicroService implementation and registration to Discovery Server ###

Now let's implement our microservice. First thing what you need to do is to say the Spring Boot application where to register your microservice, where the discovery server is running, in my case:

```
spring.application.name=personsService
eureka.client.serviceUrl.defaultZone=http://localhost:9761/eureka
```

Also notice that we called our microservice with name of "**personsService**". 
Now let's say that my microservice is going to be simple rest controller returning some JSON:


```
@RestController
public class PersonsController {
		
    @RequestMapping("/persons")
    public Persons getPersonById() {
    	final Persons persons = new Persons();
    	final Person person = new Person();
    	person.setName("Tomas");
    	person.setSurname("Kloucek");
    	person.setDepartment("Programmer");
    	persons.getPersons().add(person);
    	
    	return persons;
    }
}
```

### Client of Spring Cloud MicroService ###

Of course, you can access previous Microservice directly with browser by entering direct URL http://localhost:8080/persons or by using RestTemplate. But you'd be stupid to do so. Much more smarter is to make your client accessing the discovery server first to get the URL of the MicroService by asking Ribbon loadbalancer via MicroService id (in my case it's **personsService**) and then call the service. See the client code:

First we need to have our client to be discovery server client. 
Hence EnableDiscoveryClient annotation.

```
@SpringBootApplication
@EnableDiscoveryClient
public class SpringMicroserviceClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMicroserviceClientApplication.class, args);
	}
}
```
Then we need to ask Ribbon loadbalancer to choose one of the instance of MicroService by providing the service ID.


```
@Component
public class MicroServiceClient implements CommandLineRunner {
    
    @Autowired
    private LoadBalancerClient loadBalancer;
	
	@Override
	public void run(String... arg0) throws Exception {
		final RestTemplate restTemplate = new RestTemplate();
		
		final ServiceInstance serviceInstance = loadBalancer.choose("personsService");
		if (serviceInstance != null) {
			System.out.println("Invoking instance at URL: "+serviceInstance.getUri());
			System.out.println(
				restTemplate.getForObject(serviceInstance.getUri()+"/persons", String.class));
		} else {
			System.out.println("Didn't find any running instance of personsService at DiscoveryServer!");
		}
	}
}

```
**LoadBalancerClient** will choose one of the running instances of personsService MicroService registered at Discovery server. Sweet, isn't it?

## Running the demo ##

**Running the discovery server**

* mvn clean install (in the spring-microservice-registry directory with pom.xml)
* java -jar target/demo-0.0.1-SNAPSHOT.war
* visit http://localhost:9761 

**Running the MicroService**

* mvn clean install (in the spring-microservice-service directory with pom.xml)
* java -jar target/demo-0.0.1-SNAPSHOT.war
* Now rewresh the http://localhost:9761 page and you should see MicroService registered with discovery server.

**Running the client**

* mvn clean install (in the spring-microservice-client directory with pom.xml)
* java -jar target/demo-0.0.1-SNAPSHOT.war
* At the standard output you should see:

```
.
.
.
Invoking instance at URL: http://N1309.hcg.homecredit.net:8080
2016-11-01 11:09:45.465  INFO 3716 --- [erListUpdater-0] c.netflix.config.ChainedDynamicProperty  : Flipping property: personsService.ribbon.ActiveConnectionsLimit to use NEXT property: niws.loadbalancer.availabilityFilteringRule.activeConnectionsLimit = 2147483647
{"persons":[{"name":"Tomas","surname":"Kloucek","department":"Programmer"}]}
2016-11-01 11:09:45.961  INFO 3716 --- [           main] c.e.SpringMicroserviceClientApplication  : Started SpringMicroserviceClientApplication in 10.801 seconds (JVM running for 11.254)
```

And that's it! I didn't provide everything in this demo, but this should help you to start with Spring Cloud microservices..:)

cheers

Tomas